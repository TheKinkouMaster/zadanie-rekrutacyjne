﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// this class will store all static informations used in demo (item stats, cash, other values)
/// In real game those values should be replaced with references to game logic elements (such as player,
/// list of avaiable items etc.) so I admit that this is not optimal, low performance version made
/// just to make UI work properly.
/// </summary>
public class GameValues : MonoBehaviour
{
    #region variables
    public static int playerSoftCurrency = 10232;
    public static int playerHardCurrency = 6993;
    public Vector2Int rodPoolSize;
    //static lists used by other objects in program
    public static List<Rod> rodsStatic;
    public static List<Sprite> backgroundsStatic;
    public static List<Sprite> symbolsStatic;
    public static List<Sprite> iconsStatic;
    public static List<GameObject> modelsRodsStatic;
    //non-static lists that are transfered into static after game launches
    public List<Sprite> icons;
    public List<Sprite> backgrounds;
    public List<Sprite> symbols;
    public List<GameObject> modelsRods;
    #endregion

    #region Unity inherited methods
    #endregion

    #region MonoBehaviour inherited methods
    private void Awake()
    {
        CreateRodPool();                    //creating random rods to show in selection panel
        backgroundsStatic = backgrounds;    //attaching lists (populated in unity editor) to static ones
        symbolsStatic = symbols;
        iconsStatic = icons;
        modelsRodsStatic = modelsRods;
    }
    #endregion
    #region Methods
    /// <summary>
    /// Searches list of rod icons and returns one with matching name.
    /// </summary>
    /// <param name="sprtName">Name of needed icon file</param>
    /// <returns>Returns null in file not found</returns>
    public static Sprite GetRodIcon(string sprtName)
    {
        Sprite retval = null;
        foreach (Sprite sprt in iconsStatic)
        {
            if (sprtName == sprt.name)
            {
                retval = sprt;
                break;
            }
        }
        if(retval == null) Debug.LogWarning("Sprite not found. Sprite name: " + sprtName);
        return retval;
    }
    /// <summary>
    /// Searches list of rod models and returns one with matching name.
    /// </summary>
    /// <param name="rodModelName">Name of needed model file</param>
    /// <returns>Returns null in file not found</returns>
    public static GameObject GetRodModel(string rodModelName)
    {
        GameObject retval = null;
        foreach (GameObject model in modelsRodsStatic)
        {
            if (rodModelName == model.name)
            {
                retval = model;
                break;
            }
        }
        if(retval == null) Debug.LogWarning("Model not found. Sprite name: " + rodModelName);
        return retval;
    }
    /// <summary>
    /// Creates x number of rods used in this demo. Number of rods is random, between numbers specified in rodPoolSize variable
    /// </summary>
    public void CreateRodPool()
    {
        int numberOfRodsToGenerate = (int)Mathf.Lerp(rodPoolSize.x, rodPoolSize.y, Random.value); //randomizing number of rods
        rodsStatic = new List<Rod>(); //called once per game launch, so list is always null at start
        for (int i = 0; i < numberOfRodsToGenerate; i++)
        {
            //all following values were picked just for this demo and were based on attached to task graphics
            Rod rod = new Rod
            { 
                SpriteName = icons[i % icons.Count].name,
                Points = (int)(Random.value * 30),
                Level = (i % 3) + 1,
                Rarity = RandomiseRarity(i),
                ModelName = modelsRods[i % modelsRods.Count].name
            };
            rod.Name = rod.SpriteName.Split('_')[2] + i.ToString();
            rod.Type = DetermineType(rod.SpriteName);
            rodsStatic.Add(rod);
        }
    }
    /// <summary>
    /// Returns item rarity based on given number
    /// </summary>
    public Rod.ItemRarity RandomiseRarity(int seed)
    {
        switch (seed % 3)
        {
            case 0:
                return Rod.ItemRarity.blue;
            case 1:
                return Rod.ItemRarity.purple;
            case 2:
                return Rod.ItemRarity.yellow;
            default:
                return Rod.ItemRarity.blue;
        }
    }
    /// <summary>
    /// Returns model type based on icon file name
    /// </summary>
    /// <param name="iconName">Name of icon file</param>
    public Rod.ItemType DetermineType(string iconName)
    {
        string type = iconName.Split('_')[1]; //considering naming style of icon files, this work should be coresponding to rod type
        if (type == "spinning") return Rod.ItemType.Spinning;
        else if (type == "flyfishing") return Rod.ItemType.flyFishing;
        else if (type == "baitcasting") return Rod.ItemType.baitCasting;
        else
        {
            Debug.LogWarning("DetermineType failed. iconName: " + iconName);
            return Rod.ItemType.baitCasting;
        }
    }
    /// <summary>
    /// Returns name of rod type for use on UI Texts
    /// </summary>
    public static string RodTypeToString(Rod.ItemType type)
    {
        if (type == Rod.ItemType.baitCasting) return "BAIT CASTING";
        if (type == Rod.ItemType.flyFishing) return "FLYFISHING";
        if (type == Rod.ItemType.Spinning) return "SPINNING";
        return "NOT DEFINED";
    }
    /// <summary>
    /// Returns points required to advance to another level
    /// </summary>
    public static int GetLevelMaxPoints(int level)
    {
        switch (level)
        {
            case 1:
                return 4;
            case 2:
                return 10;
            case 3:
                return 20;
            default:
                return 0;
        }
    }
    #endregion
}
/// <summary>
/// Class with all informations needed to get UI elements working. In game I imagine those informations would be packed
/// into more efficient object, but for this demo I just made it to work fine
/// </summary>
public class Rod
{
    #region Variables
    private string name;
    private string spriteName;
    private string modelName;
    private int points;
    private int level;
    private ItemRarity rarity;
    private ItemType type;
    #endregion
    #region Accessors
    public string Name { get => name; set => name = value; }
    public int Points { get => points; set => points = value; }
    public int Level { get => level; set => level = value; }
    public string SpriteName { get => spriteName; set => spriteName = value; }
    public ItemRarity Rarity { get => rarity; set => rarity = value; }
    public ItemType Type { get => type; set => type = value; }
    public string ModelName { get => modelName; set => modelName = value; }
    #endregion
    #region enums and structs
    public enum ItemRarity
    {
        blue, purple, yellow
    }
    public enum ItemType
    {
        baitCasting, flyFishing, Spinning
    }
    #endregion
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// This class will control main rod preview window and everything that happens on it
/// </summary>
public class PreviewWindow : MonoBehaviour
{
    #region Variables
    public Transform rodAnchor; //to this object rod model will be attached
    public GameObject currentRod; //current rod model attached to anchor
    public SelectionWindow selectedRodWindow; //one of two panels with informations. This one corresponds to rod
    private Rod rod; //data about current rod
    #endregion
    #region Accessors
    public Rod Rod { get => rod; set => rod = value; }
    #endregion
    #region Methods
    /// <summary>
    /// Sets given rod as current, updates information panels and swaps rod models
    /// </summary>
    public void ChangeRod(Rod newRod)
    {
        Rod = newRod;
        selectedRodWindow.InitWindow(Rod);
        if(currentRod != null) Destroy(currentRod); 
        currentRod = Instantiate(GameValues.GetRodModel(Rod.ModelName),rodAnchor);
    }
    #endregion
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
/// <summary>
/// Since this type of progress bar (level, level sprite, custom text in the middle) appears in many different places
/// this class will control it's behaviour, instead of copying same methods across couple of other classes
/// </summary>
public class ProgressBar : MonoBehaviour
{
    #region Variables
    public Slider progressBar;
    public TMP_Text textValue;
    public TMP_Text textLevel;
    public Image LevelImage;
    public GameObject UpgradeArrow;
    #endregion
    #region Methods
    /// <summary>
    /// Changes value of current slider based on given values. Also updates level text
    /// </summary>
    /// <param name="currentValue">Current progress</param>
    /// <param name="maxValue">Max progress</param>
    /// <param name="newLevel">Item Level</param>
    public void ChangeValue(float currentValue, float maxValue, int newLevel)
    {
        progressBar.value = Mathf.Lerp(0, 1, currentValue / maxValue);
        textValue.text = currentValue.ToString() + "/" + maxValue.ToString();
        textLevel.text = newLevel.ToString();
        //updating arrow status
        if (progressBar.value >= 1) UpgradeArrow.SetActive(true);
        else UpgradeArrow.SetActive(false);
    }
    /// <summary>
    /// Changes sprite behind level text
    /// </summary>
    public void ChangeGraphics(Sprite newGraphics)
    {
        LevelImage.sprite = newGraphics;
    }
    #endregion
}

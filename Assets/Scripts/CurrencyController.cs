﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

/// <summary>
/// This class controlls behaviour of both currency bars.
/// Gets amount of cash player has, updates bar values and responds to button clicking
/// </summary>
public class CurrencyController : MonoBehaviour
{
    #region variables
    private int playerSoftCurrency;
    private int playerHardCurrency;

    public TMP_Text textSoftCurrency;
    public TMP_Text textHardCurrency;
    #endregion

    #region accessors
    public int PlayerSoftCurrency { get => playerSoftCurrency; set => playerSoftCurrency = value; }
    public int PlayerHardCurrency { get => playerHardCurrency; set => playerHardCurrency = value; }
    #endregion

    #region Unity inherited methods
    private void Awake()
    {
        UpdateCurrencyValues();
        UpdateCurrencyBars();
    }
    private void Update()
    {
        ///in this demo player currency is fixed, so there is no need to update UI every frame.
        ///In game (when currency values actually change) this can be uncommented, however it
        ///might be exhausting too much resources to call it every frame, so UpdateCurrencyBars
        ///method should be called from within game logic when player gains or loses money.
        //UpdateCurrencyBars();
    }
    #endregion

    #region Methods
    public void UpdateCurrencyBars()
    {
        textSoftCurrency.text = PlayerSoftCurrency.ToString();
        textHardCurrency.text = PlayerHardCurrency.ToString(); ;
    }
    public void UpdateCurrencyValues()
    {
        PlayerHardCurrency = GameValues.playerHardCurrency;
        PlayerSoftCurrency = GameValues.playerSoftCurrency;
    }
    /// <summary>
    /// temporary method just to test how options button work. In game content should be replaced
    /// with something to change screen into options.
    /// </summary>
    public void EnterOptions()
    {
        Debug.Log("Options");
    }
    /// <summary>
    /// temporary method that changed amount of soft currency player has. In full game should be
    /// linked to game logic that will handle all mechanics behind this action
    /// </summary>
    public void AddSoftCurrency()
    {
        GameValues.playerSoftCurrency += 100;
        UpdateCurrencyValues();
        UpdateCurrencyBars();
    }
    /// <summary>
    /// temporary method that changed amount of hard currency player has. In full game should be
    /// linked to game logic that will handle all mechanics behind this action
    /// </summary>
    public void AddHardCurrency()
    {
        GameValues.playerHardCurrency += 100;
        UpdateCurrencyValues();
        UpdateCurrencyBars();
    }
    #endregion
}

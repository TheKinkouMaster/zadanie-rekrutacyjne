﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class UIController : MonoBehaviour
{
    #region Variables
    public RectTransform rectPreviewWindow; //Rect of main preview window at the top of screen
    public Transform uiAnchor; //reference to content of scroll view
    public PreviewWindow previewWindow;
    public GameObject prefabTextTitle;
    public GameObject prefabSelectWindow;
    private List<SelectionWindow> selectionWindows;
    private SelectionWindow currentlySelectedWindow;
    private Vector2 spawnPosition;
    private int spawningIndex;
    #endregion
    #region MonoBehaviour inherited methods
    private void Start()
    {
        SpawnWindows();
    }
    #endregion
    #region Methods
    /// <summary>
    /// Spawns all three categories of rods with all rods listed in GameValues put in selectionWindows
    /// </summary>
    public void SpawnWindows()
    {
        selectionWindows = new List<SelectionWindow>(); //since this method is called once, selectionWindows is always null at start
        spawnPosition = new Vector2(); //vector with informations where to position objects on canvas
        float windowWidth = rectPreviewWindow.rect.width / 4; //what is a width of single selection window?
        spawningIndex = 0; //index of currently generated window

        //starting spawning spot should be right below PreviewWindow
        spawnPosition.y -= rectPreviewWindow.rect.height;
        SpawnCategory("Spinning", FindRodsOfType(Rod.ItemType.Spinning),windowWidth);
        SpawnCategory("Flyfishing", FindRodsOfType(Rod.ItemType.flyFishing), windowWidth);
        SpawnCategory("Baitcasting", FindRodsOfType(Rod.ItemType.baitCasting), windowWidth);
        spawnPosition.y -= 150;
        //resizing content window to match amount of selection windows
        uiAnchor.GetComponent<RectTransform>().sizeDelta = new Vector2(uiAnchor.GetComponent<RectTransform>().sizeDelta.x, -spawnPosition.y);
        //selecting first object. In game that would be saved from previous game, but this is only demo
        SelectWindow(selectionWindows[0]);
    }
    /// <summary>
    /// Spawns group of rods containing the same category
    /// </summary>
    /// <param name="category">Header for group of rods</param>
    /// <param name="rodsToDisplay">List of Rod data objects</param>
    /// <param name="windowWidth">Width of single selection window</param>
    public void SpawnCategory(string category, List<Rod> rodsToDisplay, float windowWidth)
    {
        //if list of rods containst at least one object, spawn header with title
        if(rodsToDisplay.Count != 0)
        {
            GameObject header = Instantiate(prefabTextTitle, uiAnchor);
            header.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, spawnPosition.y);
            header.GetComponentInChildren<TMP_Text>().text = category;
            spawnPosition.y -= header.GetComponent<RectTransform>().rect.height;
        }
        //spawning selection windows
        for (int i = 0; i < rodsToDisplay.Count; i++)
        {
            //Create object
            GameObject window = Instantiate(prefabSelectWindow, uiAnchor);
            //Move window to right spot, resize, update data visible on screen and assign this as UI controller
            window.GetComponent<RectTransform>().anchoredPosition = new Vector2(windowWidth * (spawningIndex), spawnPosition.y);
            SelectionWindow selectionWindow = window.GetComponent<SelectionWindow>();
            selectionWindow.ResizeHorizontally(windowWidth);
            selectionWindow.InitWindow(rodsToDisplay[i]);
            selectionWindow.uiController = this;
            //adding this window to window list, increasing index and moving spawning position if reached the end of a row
            selectionWindows.Add(window.GetComponent<SelectionWindow>());
            spawningIndex++;
            if (spawningIndex >= 4)
            {
                spawningIndex -= 4;
                spawnPosition.y -= 150;
            }
        }
        //if last row wasn't complete (didn't have 4 windows) reset spawning index and move spawning position
        if (spawningIndex != 0)
        {
            spawningIndex = 0;
            spawnPosition.y -= 150;
        }

    }
    /// <summary>
    /// Returns Rod objects of the type given in parameters
    /// </summary>
    /// <param name="type">Searched type</param>
    public List<Rod> FindRodsOfType(Rod.ItemType type)
    {
        List<Rod> retval = new List<Rod>();
        foreach (Rod rod in GameValues.rodsStatic)
            if (rod.Type == type) retval.Add(rod);
        return retval;
    }
    /// <summary>
    /// Marks given window as selected and updates preview window with it's data
    /// </summary>
    public void SelectWindow(SelectionWindow windowToSelect)
    {
        if(currentlySelectedWindow != null) currentlySelectedWindow.Select(false);
        currentlySelectedWindow = windowToSelect;
        currentlySelectedWindow.Select(true);
        previewWindow.ChangeRod(currentlySelectedWindow.Rod);
    }
    #endregion
}
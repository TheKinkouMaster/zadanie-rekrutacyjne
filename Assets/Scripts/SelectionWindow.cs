﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

/// <summary>
/// Class that controls behaviour of selection windows with possible rod models to attach to currently used one
/// </summary>
public class SelectionWindow : MonoBehaviour
{
    #region Variables
    private Rod rod;

    public TMP_Text textName;
    public TMP_Text textType;
    public Image imageRod;
    public Image backgroundImage;
    public GameObject selectedImage;
    public UIController uiController;
    public ProgressBar progressBar;
    #endregion

    #region Accessors
    public Rod Rod { get => rod; set => rod = value; }
    #endregion

    #region Methods
    /// <summary>
    /// Initiates window and updates all necessary data (name, type, progress bar)
    /// </summary>
    public void InitWindow(Rod rodData)
    {
        Rod = rodData;
        textName.text = Rod.Name;
        if (textType != null) textType.text = GameValues.RodTypeToString(rod.Type);
        progressBar.ChangeValue(Rod.Points, GameValues.GetLevelMaxPoints(Rod.Level), Rod.Level);
        AdjustGraphics();
    }
    /// <summary>
    /// Turns on/off "Selected" bar 
    /// </summary>
    public void Select(bool isSelected)
    {
        selectedImage.SetActive(isSelected);
    }
    /// <summary>
    /// For button use; sends information to UI controller to change current rod;
    /// </summary>
    public void ActionSelect()
    {
        uiController.SelectWindow(this);
    }
    /// <summary>
    /// Resizes window (considering scale) and repositions it to current anchor
    /// </summary>
    /// <param name="newWidth">New width</param>
    public void ResizeHorizontally(float newWidth)
    {
        float width = newWidth * (1 / backgroundImage.transform.localScale.x);
        backgroundImage.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, width);
        backgroundImage.GetComponent<RectTransform>().anchoredPosition = new Vector2(newWidth / 2, backgroundImage.GetComponent<RectTransform>().anchoredPosition.y);
    }
    /// <summary>
    /// rechecks if all graphics on screen correspod to specified in data class.
    /// Because it's demo version I made it just so it all works (GameValues class).
    /// </summary>
    public void AdjustGraphics()
    {
        int index = 0; //index of needed sprites and files in GameValues lists
        //updating rod image
        if (imageRod != null) imageRod.sprite = GameValues.GetRodIcon(Rod.SpriteName); 
        //finding index value
        if (Rod.Rarity == Rod.ItemRarity.blue) index = 0;
        else if (Rod.Rarity == Rod.ItemRarity.purple) index = 1;
        else if (Rod.Rarity == Rod.ItemRarity.yellow) index = 2;
        //if following components are attached - update them
        if (backgroundImage != null) backgroundImage.sprite = GameValues.backgroundsStatic[index];
        if (progressBar != null) progressBar.ChangeGraphics(GameValues.symbolsStatic[index]);
    }
    #endregion
}

# Zadanie rekrutacyjne
## Ogólne uwagi
Ze względu na brak doświadczenia w game devie nie byłem pewien jaką konwencje przyjać przy pisaniu programu. 
Moje nawyki to połączenie tego co zostało mi wpojone na studiach, tego co zobaczyłem w internetowych poradnikach, dokumentacja i przykładach, oraz własnego 
doświadczenia (niektóre praktyki po prostu bardzo ułatwiały mi odnalezienie się w kodzie). Jeśli więc w praktyce stosuje się inne konwencje to w przypadku 
ewentualnego zatrudnienia nie będę miał problemu z przestawieniem się. 
### Styl kodu
Nie byłem pewien jak powinno się nazywać kolejne metody/atrybuty/klasy. Przywykłem do używania prefiksu m_ przy atrybutach, 
jednak prawie nie spotkałem się z tym w kursach internetowych więc nie stosowałem tego w tym przypadku. 
Podobnie spotkałem się ze stosowaniem prefiksów przy plikach takich jak Cs, Pf, Anim, etc., ale przy innych rozmowach kwalifikacyjnych 
takie nazywanie plików było uznane za niedopuszczalne, więc w tym przypadku użyłem takie konwencji jaką uznałem za stosowną.

Wszystkie komentarze w kodzie, jak również nazwy atrybutów, klas i metod są ustalone w oparciu o język angielski.

Ostatnią rzeczą jest stosowanie interfejsów. Tylko raz się spotkałem z przypadkiem gdzie wszystkie rzeczy dotyczące Unity (komponenty Text, Image, wszystko co możliwe do ustalenia
w edytorze) były umiejscowione w interfejsie do prawidłowego obiektu, w których wszystkie atrybuty były poddane enkapsulacji, tworząc dwa poziomy - logiki i Unity, które się
ze sobą komunikowały przez te interfejsy. Jest to rzecz którą widziałem tylko raz, dlatego również tego nie stosowałem w tym programie.
### Styl projektu
Spotkałem się z tym aby każdy obiekt na scenie umieszczać w pustym obiekcie "Application", aby kontrolować to co się pojawia na scenie, jak również z konwencjami
które pozwalały na umieszczanie wszystkiego wprost na scenie lub nawet tworzenie pustych obiektów o nazwie "-----UI-----" aby tworzyć grupy obiektów. Przyjąłem wersje pierwszą
ze względu największą czytelność.
### Problemy z fontem
Załączony do assetów font nie zawierał polskich znaków, więc przetłumaczyłem zawartość makiety na angielski. Po otrzymaciu fonta z polskimi znakami nie ma problemu aby
przetłumaczyć wszystko na polski.
## Wytyczne
### Wersja Unity
Program powstał na wersji 2019.2.0f1
### Aspekty ekranu
Przetestowałem większość aspektów ekranu z zadanego przedziału. Zmiana aspektu powoduje lekkie przesunięcie się granicy dolnej części ekranu, jednak dla każdego zadanego aspektu
wszystkie wędki da się wybrać poprawnie. Ponieważ na makiecie dołączonej do zadania również widać nieznacznej wielkości pusty pasek u dołu ekranu postanowiłem go zostawić, 
gdyż nie blokuje to niczego na ekranie
### Font
Użyty font to dokładnie ten sam co wysłany w zadaniu, jednak przekonwertowany na wersje obsługiwaną przez TextMeshPro, przy użyciu narzędzi tego pakietu. Użyłem TextMeshPro z
dwóch powodów wymienionych w dalszej części tego pliku
### Parametry wędki
Większość parametrów jest wpisana na stałe. Implementacja ich dynamiki polegałaby po prostu na dołączeniu kolejnych Sliderów do okna podglądu i uzależnienia ich
położenia od danych konketnej wędki. Ponieważ nie trzeba było tego implementować, zostawiłem to jako dane statyczne
### Użyte zewnętrzne pluginy i biblioteki
TextMeshPro oraz pakiet do edycji spritów (był potrzebny przy robieniu 9-slicingu)
## Wersja podstawowa
### Scroll 
Scroll nie obejmuje jedynie pasków twardej i miękkiej waluty, oraz przycisku opcji na górze ekranu.
### Wędka
Wędka scrolluje się z resztą okna i z tego co zauważyłem, na załączonej do zadania grafice wędka wystaje poza okno podglądu, więc podobnie umieściłem ją przed obiektem canvas
aby uzyskać ten efekt. Można też spróbować wyrenderować teksture przy użyciu drugiej kamery i tą teksture umieścić na ekranie, jednak z tego co wiem jest to metoda
obciążająca procesor więc ze względu na docelową platformę aplikacji nie użyłem tej metody.
### Wybór wędek
Wędki mają nazwy które wywodzą się z nazwy ich plików oraz numeru dotychczas stworzonych wędek aby nadać każdej unikalność i zachować sens.
### Podmiana wędek
Podmiana wędek działa na zasadzie zniszczenia poprzedniego modelu i utworzenia nowego. Można byłoby na początku działania programu wygenerować wszystkie modele, a potem
włączać lub wyłączać kolejne z nich, jednak to zwiększałoby zużycie pamięci. Metoda której użyłem obciąża procesor w momencie niszczenia/tworzenia obiektu jednak ze względu
na to, że są to pojedyńcze obiekty w momencie kiedy w tle nic się nie dzieje, wydało mi się dosyć optymalnym rozwiązaniem
### Modyfikowanie
Żadna grafika nie została zmodyfikowana. Jedynie poddano je 9-slicingu w samym Unity aby osiągnąć wynik zbliżony do podanej makiety
## Zadania dodatkowe
### Inicjalizowanie okna
Okno tworzy się z dowolną ilością wędek podaną w obiekcie GameLogic. Sprawdzone w zakresie 1-100. Wszystkie parametry wędek są dobierane według
mojego własnego pomysłu aby była pewna losowość.
### Optymalizacja
Optymalizowałem okno poprzez użycie metod wymienionych wcześniej, oraz przy użyciu outline'u TextMeshPro (który według różnych danych jest wydajniejszy niż standardowy outline
Unity trzy do pięciu razy)
### Outline
Zbliżony efekt udało mi się uzyskać przez użycie TextMeshPro. Pytanie pozostaje czy ten outline zadziała dla języka chińskiego - według dokumentacji TextMeshPro ten język
i wiele innych jest wspieranych, o ile plik fonta zawiera te znaki, więc wydaje się, że odpowiedź to tak, jednak nie przetestowałem tego ponieważ w pliku fonta nie ma takich
znaków
